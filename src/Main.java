import Model.SlangWord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, String> slangDic= new HashMap<>();
        HashMap<String, String> masterslangDic= new HashMap<>();
        List<SlangWord> historySlangSearch = new ArrayList<>();
        List<String> listSlangWord = new ArrayList<>();
        //Read file
        try {
            BufferedReader readFileSlang= new BufferedReader(new FileReader("./slang.txt"));

            int slangCount= 0;
            String line;
            while ((line = readFileSlang.readLine()) != null) {
                String[] splitArr = line.split("`");
                if(splitArr.length > 1) {
                    //Add vao list slangDic
                    slangDic.put(splitArr[0], splitArr[1]);
                    listSlangWord.add(splitArr[0]);
                    slangCount++;
                }
                else
                    slangDic.put(splitArr[0], "");
                    listSlangWord.add(splitArr[0]);
                slangCount++;
            }
            readFileSlang.close();
            masterslangDic = slangDic;
            System.out.print(masterslangDic);

            //Search slang
            Integer chooseMenu;
            chooseMenu = 0;

            while (chooseMenu == 0) {
                System.out.println("***. SLANG WORD PROJECT****");
                System.out.println("1. Search by slang word");
                System.out.println("2. Search by define, Show list slang word define. ");
                System.out.println("3. Show history list slang word ");
                System.out.println("4. Add new slang word. If duplicate confirm overwrite or duplicate new slang word");
                System.out.println("5. Edit 1 slang word");
                System.out.println("6. Delete 1 slang word. Confirm before delete");
                System.out.println("7. Reset list slang word");
                System.out.println("8. Random 1 slang word");
                System.out.println("9. Funny question: Show 1 random slang word and 4 answer for user choose");
                System.out.println("10.Funny question");

                chooseMenu = Integer.parseInt(reader.readLine());

                if (chooseMenu == 1) {
                    int continueSearchSlang = 1;
                    while (continueSearchSlang == 1) {
                        System.out.println("Search slang word:");
                        String search = reader.readLine().toUpperCase();
                        String resultMean = slangDic.get(search);

                        //Add slang word into History Slang
                        SlangWord slangWord = new SlangWord();
                        slangWord.setWord(search);
                        slangWord.setMean(resultMean);
                        historySlangSearch.add(slangWord);

                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");

                        System.out.println("Result mean " + search + " is " + resultMean);

                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueSearchSlang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueSearchSlang;
                    }

                } else if (chooseMenu == 2) {
                    int continueSearchMean = 1;
                    while (continueSearchMean == 1) {
                        System.out.println("Search definition keyword:");
                        String mean = reader.readLine().toUpperCase();

                        System.out.println("Word contain :");
                        //Add slang word into History Slang
                        for (Map.Entry<String, String> entry : slangDic.entrySet()) {
//                            System.out.println(entry.getKey() + " - " + entry.getValue());
                            if(entry.getValue().toUpperCase().contains(mean)) {
                                System.out.print(entry.getKey()+ ",");
                            }
                        }

                        System.out.println();
                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueSearchMean = Integer.parseInt(reader.readLine());
                        chooseMenu = continueSearchMean;
                    }
                } else if (chooseMenu == 3) {
                    int continueHistorySlang = 1;
                    while (continueHistorySlang == 1) {
                        System.out.println("3. History search list Slang word ");
                        for (SlangWord slangWord :
                                historySlangSearch) {
                            System.out.println("Word:  " + slangWord.getWord() + " , Mean: " + slangWord.getMean());

                            System.out.println("Do you want continue show history? If yes choose 1, no choose 0");
                            continueHistorySlang = Integer.parseInt(reader.readLine());
                            chooseMenu = continueHistorySlang;
                        }
                    }
                } else if (chooseMenu == 4) {
                    int continueAddSlang = 1;
                    while (continueAddSlang == 1) {
                        System.out.println("4. Add new Slang word ");
                        System.out.println("- Slang word:");
                        String word = reader.readLine().toUpperCase();
                        String valueExist= slangDic.get(word);
                        if(valueExist == null) {
                            System.out.println("- Mean:");
                            String mean = reader.readLine();
                            slangDic.put(word, mean);
                            System.out.println("Add slang word success!");
                            System.out.println("Word new: "+ word+ ", Mean: " + slangDic.get(word));

                        }else {
                            System.out.println("Word "+ word + " is exist!");
                            System.out.println("Do you want Overwrite or Duplicate slang word? If yes choose 2 to Overwrite, 3: Duplicate");
                            //Handle Overwrite, Duplicate
                            int option = Integer.parseInt(reader.readLine());
                            if(option == 2) {
                                System.out.println("Overwrite: Please input Mean of "+ word + " is:");
                                String meanOverite = reader.readLine().toUpperCase();
                                slangDic.put(word, meanOverite);
                                System.out.println("Add success! Word: "+ word + ". Mean: "+ slangDic.get(word));
                            }else if (option == 3) {
                                System.out.println("Duplicate: Please input Mean of "+ word + " is:");
                                String meanInputDuplicate = reader.readLine().toUpperCase();
                                String meanOriginal = slangDic.get(word);
                                slangDic.put(word, meanOriginal+ ", "+ meanInputDuplicate);
                                System.out.println("Add success! Word: "+ word + ". Mean: "+ slangDic.get(word));
                            }
                        }

                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueAddSlang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueAddSlang;
                    }
                }else if (chooseMenu == 5) {
                    int continueEditSlang = 1;
                    while (continueEditSlang == 1) {
                        System.out.println("5. Edit Slang word: ");
                        System.out.println("- Word: ");
                        String word = reader.readLine().toUpperCase();
                        String valueExist= slangDic.get(word).toUpperCase();
                        if(valueExist != null) {
                            System.out.println("- Mean want edit: ");
                            String meanEdit= reader.readLine();
                            slangDic.put(word, meanEdit);
                            System.out.println("- Edit word : "+ word +". Mean: "+ slangDic.get(word)+ " success!");
                        }else {
                            System.out.println("Word: "+ word + " is not exist in Slang word to Edit");
                        }

                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueEditSlang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueEditSlang;
                    }
                } else if (chooseMenu == 6) {
                    int continueDeletelang = 1;
                    while (continueDeletelang == 1) {
                        System.out.println("6. Delete Slang word: ");
                        System.out.println("- Word to delete:");
                        String word = reader.readLine().toUpperCase();
                        String valueExist= slangDic.get(word);
                        if(valueExist != null) {
                            System.out.println("Do you want to delete: "+ word + "? If yes choose 1, no choose 0");
                            int isDelete = Integer.parseInt(reader.readLine());
                            if(isDelete == 1) {
                                slangDic.remove(word);
                                System.out.println("- If exist : "+ slangDic.get(word));
                            }
                        }else {
                            System.out.println("Word: "+ word + " is not exist in Slang word to Remove");
                        }

                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueDeletelang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueDeletelang;
                    }
                }else if (chooseMenu == 7) {
                    int continueResetSlang = 1;
                    while (continueResetSlang == 1) {
                        System.out.println(slangDic.get("GAG"));
                        slangDic.clear();
                        slangDic = masterslangDic;
                        System.out.println(masterslangDic);
                        System.out.println("6. Reset list Slang word success! ");
                        System.out.println(slangDic.get("GAG"));

                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueResetSlang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueResetSlang;
                    }
                }else if (chooseMenu == 8) {
                    int continueRandomSlang = 1;
                    while (continueRandomSlang == 1) {
                        Random random = new Random();
                        String randomElement = listSlangWord.get(random.nextInt(listSlangWord.size()));
                        System.out.println("8. Slang word random : " + randomElement+ ", Mean: "+ slangDic.get(randomElement));
                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueRandomSlang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueRandomSlang;
                    }
                }else if (chooseMenu == 9) {
                    int continueQuizzRandomSlang = 1;
                    while (continueQuizzRandomSlang == 1) {
                        Random random = new Random();
                        String randomElement = listSlangWord.get(random.nextInt(listSlangWord.size()));
                        String randomA = listSlangWord.get(random.nextInt(listSlangWord.size()));
                        String randomB = listSlangWord.get(random.nextInt(listSlangWord.size()));
                        String randomC = listSlangWord.get(random.nextInt(listSlangWord.size()));
                        System.out.println("8. Word : " + randomElement+ ", Mean ?");

                        String masterMean = slangDic.get(randomElement);
                        List<String> givenList = Arrays.asList(slangDic.get(randomA), slangDic.get(randomB), slangDic.get(randomC), masterMean);
                        String randomMean = givenList.get(random.nextInt(givenList.size()));

                        System.out.println("A. "+ randomMean);
                        givenList.remove(randomMean);
                        String randomMeanB = givenList.get(random.nextInt(givenList.size()));
                        System.out.println("B. "+ randomMeanB);
                        givenList.remove(randomMeanB);

                        String randomMeanC = givenList.get(random.nextInt(givenList.size()));
                        System.out.println("C. "+ randomMeanC);
                        givenList.remove(randomMeanB);
                        System.out.println("D. "+ givenList.get(0));

                        System.out.println("Answer: ");
                        String optionInput = reader.readLine().toUpperCase();


                        if(optionInput.equals("A")) {
                            if(masterMean.equals(slangDic.get(randomA))){
                                System.out.println("Correct!");
                            }else {
                                System.out.println("InCorrect!");
                            }
                        }else if (optionInput.equals("B")) {
                            if(masterMean.equals(slangDic.get(randomB))){
                                System.out.println("Correct!");
                            }else {
                                System.out.println("InCorrect!");
                            }
                        }else if (optionInput.equals("C")) {
                            if(masterMean.equals(slangDic.get(randomC))){
                                System.out.println("Correct!");
                            }else {
                                System.out.println("InCorrect!");
                            }
                        }else if (optionInput.equals("D")) {
                            if(masterMean.equals(slangDic.get(randomElement))){
                                System.out.println("Correct!");
                            }else {
                                System.out.println("InCorrect!");
                            }
                        }
                        System.out.println("8. Word : " + randomElement+ ", Mean: "+ masterMean);

                        System.out.println("Do you want continue search slang? If yes choose 1, no choose 0");
                        continueQuizzRandomSlang = Integer.parseInt(reader.readLine());
                        chooseMenu = continueQuizzRandomSlang;
                    }
                }else if (chooseMenu == 10) {

                }
            }
        }catch (Exception e) {
            System.out.println("ERROR "+ e);
        }
    }
}
